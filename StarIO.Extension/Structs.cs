﻿using System;
using ObjCRuntime;

namespace StarIO_Extension
{

[Native]
public enum SCBInitializationType : long
{
    SCBInitializationTypeCommand
}

[Native]
public enum SCBFontStyleType : long
{
    A,
    B
}

[Native]
public enum SCBCodePageType : long
{
    Cp437,
    Cp737,
    Cp772,
    Cp774,
    Cp851,
    Cp852,
    Cp855,
    Cp857,
    Cp858,
    Cp860,
    Cp861,
    Cp862,
    Cp863,
    Cp864,
    Cp865,
    Cp866,
    Cp869,
    Cp874,
    Cp928,
    Cp932,
    Cp998,
    Cp999,
    Cp1001,
    Cp1250,
    Cp1251,
    Cp1252,
    Cp2001,
    Cp3001,
    Cp3002,
    Cp3011,
    Cp3012,
    Cp3021,
    Cp3041,
    Cp3840,
    Cp3841,
    Cp3843,
    Cp3844,
    Cp3845,
    Cp3846,
    Cp3847,
    Cp3848,
    Utf8,
    Blank
}

[Native]
public enum SCBInternationalType : long
{
    Usa,
    France,
    Germany,
    Uk,
    Denmark,
    Sweden,
    Italy,
    Spain,
    Japan,
    Norway,
    Denmark2,
    Spain2,
    LatinAmerica,
    Korea,
    Ireland,
    Legal
}

[Native]
public enum SCBLogoSize : long
{
    Normal,
    DoubleWidth,
    DoubleHeight,
    DoubleWidthDoubleHeight
}

[Native]
public enum SCBAlignmentPosition : long
{
    Left,
    Center,
    Right
}

[Native]
public enum SCBCutPaperAction : long
{
    FullCut,
    PartialCut,
    FullCutWithFeed,
    PartialCutWithFeed
}

[Native]
public enum SCBPeripheralChannel : long
{
    SCBPeripheralChannelNo1,
    SCBPeripheralChannelNo2
}

[Native]
public enum SCBSoundChannel : long
{
    SCBSoundChannelNo1,
    SCBSoundChannelNo2
}

[Native]
public enum SCBBarcodeSymbology : long
{
    Upce,
    Upca,
    Jan8,
    Jan13,
    Code39,
    Itf,
    Code128,
    Code93,
    Nw7
}

[Native]
public enum SCBBarcodeWidth : long
{
    SCBBarcodeWidthMode1,
    SCBBarcodeWidthMode2,
    SCBBarcodeWidthMode3,
    SCBBarcodeWidthMode4,
    SCBBarcodeWidthMode5,
    SCBBarcodeWidthMode6,
    SCBBarcodeWidthMode7,
    SCBBarcodeWidthMode8,
    SCBBarcodeWidthMode9
}

[Native]
public enum SCBPdf417Level : long
{
    SCBPdf417LevelECC0,
    SCBPdf417LevelECC1,
    SCBPdf417LevelECC2,
    SCBPdf417LevelECC3,
    SCBPdf417LevelECC4,
    SCBPdf417LevelECC5,
    SCBPdf417LevelECC6,
    SCBPdf417LevelECC7,
    SCBPdf417LevelECC8
}

[Native]
public enum SCBQrCodeModel : long
{
    SCBQrCodeModelNo1,
    SCBQrCodeModelNo2
}

[Native]
public enum SCBQrCodeLevel : long
{
    L,
    M,
    Q,
    H
}

[Native]
public enum SCBBitmapConverterRotation : long
{
    Normal,
    Right90,
    Left90,
    Rotate180
}

[Native]
public enum SCBBlackMarkType : long
{
    Invalid,
    Valid,
    ValidWithDetection
}

[Native]
public enum StarIoExtEmulation : long
{
    None = 0,
    StarPRNT,
    StarLine,
    StarGraphic,
    EscPos,
    EscPosMobile,
    StarDotImpact
}

[Native]
public enum StarIoExtCharacterCode : long
{
    None = 0,
    Standard,
    Japanese,
    SimplifiedChinese,
    TraditionalChinese
}

}
