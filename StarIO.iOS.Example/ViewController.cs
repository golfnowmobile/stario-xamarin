﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Timers;
using Foundation;
using UIKit;

namespace StarIO.iOS.Example
{
    public partial class ViewController : UITableViewController
    {

        UIRefreshControl refresh;

        private Timer _timer;

        private List<PortInfo> _printers;

        private string _printerAddress;


        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }


        public ViewController()
        {

        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();



            _printers = new List<PortInfo>();
            _timer = new Timer(1000);
            _timer.Elapsed += timer_Elapsed;
            _timer.Enabled = true;

            refresh = new UIRefreshControl();

            refresh.ValueChanged += (sender, e) =>
            {

                SearchPrinters();
                refresh.EndRefreshing();

            };

            TableView.RefreshControl = refresh;

            TableView.ContentInset = new UIEdgeInsets(20, 0, 0, 0);
            // Perform any additional setup after loading the view, typically from a nib.



            //btnPrint.TouchUpInside += (sender, e) =>
            //{
            //    StarIoExtEmulation emulation = StarIoExtEmulation.StarGraphic;

            //    ISCBBuilder builder = StarIoExt.CreateCommandBuilder(emulation);

            //    if (builder != null)
            //    {
            //        builder.BeginDocument();

            //        //builder.AppendBitmap();

            //        builder.EndDocument();


            //    }


            //};
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {



                _timer.Interval = 3000;
                _timer.Enabled = false;

                var foundPrinter = new List<PortInfo>();

                var search = SMPort.SearchPrinter();

                foreach (var printer in search)
                {
                    Console.WriteLine(printer.PortName);
                    Console.WriteLine(printer.ModelName);
                    Console.WriteLine(printer.MacAddress);
                    foundPrinter.Add(printer);
                }

                InvokeOnMainThread(() =>
                {
                    _printers.Clear();
                    _printers.AddRange(foundPrinter);

                    LoadPrinters();
                });

            }
            catch (Exception ex)
            {

                Debug.WriteLine(ex.ToString());

            }
            _timer.Enabled = true;
        }

        private void LoadPrinters()
        {
            var source = new PrinterTableSource(_printers);
            source.PrinterSelected += PrinterSelected;
            TableView.Source = source;
            TableView.ReloadData();
        }

        private void PrinterSelected(int section, int index)
        {
            _printerAddress = _printers[index].PortName.Remove(0, 4);
        }


        public void SearchPrinters()
        {
            Task.Factory.StartNew(() =>
            {

                if (_timer.Enabled)
                {

                    Debug.WriteLine("Timer Enabled.");

                }
                else
                {

                    Debug.WriteLine("Timer is not Enabled.");
                    _timer.Enabled = true;
                }

                var search = SMPort.SearchPrinter();

                foreach (var printer in search)
                {
                    Console.WriteLine(printer.PortName);
                    Console.WriteLine(printer.ModelName);
                    Console.WriteLine(printer.MacAddress);
                    //foundPrinter.Add(printer);
                }

            });


        }
        public override void RowSelected(UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            //base.RowSelected(tableView, indexPath);

            tableView.DeselectRow(indexPath, true);



        }



        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }


    class PrinterTableSource : UITableViewSource
    {
        private List<PortInfo> _printers;

        public event Action<int, int> PrinterSelected;

        public PrinterTableSource(List<PortInfo> printers)
        {
            _printers = printers;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell("cellIdentifier") ??
                       new UITableViewCell(UITableViewCellStyle.Subtitle, "cellIdentifier");
            cell.TextLabel.Text = _printers[indexPath.Row].ModelName;
            cell.DetailTextLabel.Text = _printers[indexPath.Row].PortName;

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _printers.Count;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            PrinterSelected(indexPath.Section, indexPath.Row);
        }
    }
}
