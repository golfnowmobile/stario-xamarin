﻿using System;
using ExternalAccessory;
using Foundation;
using ObjCRuntime;
using StarIO;

namespace StarIO
{



    // @interface WBluetoothPort : NSObject <NSStreamDelegate>
    [BaseType(typeof(NSObject))]
    interface WBluetoothPort : INSStreamDelegate
    {
        // @property (readonly, getter = isConnected) BOOL connected;
        [Export("connected")]
        bool Connected { [Bind("isConnected")] get; }

        // @property (readwrite) u_int32_t endCheckedBlockTimeoutMillis;
        [Export("endCheckedBlockTimeoutMillis")]
        uint EndCheckedBlockTimeoutMillis { get; set; }

        // @property (readonly, retain) NSString * firmwareInformation;
        [Export("firmwareInformation", ArgumentSemantic.Retain)]
        string FirmwareInformation { get; }

        // -(id)initWithPortName:(NSString *)portName portSettings:(NSString *)portSettings timeout:(u_int32_t)timeout;
        [Export("initWithPortName:portSettings:timeout:")]
        IntPtr Constructor(string portName, string portSettings, uint timeout);

        // -(BOOL)open;
        [Export("open")]
        bool Open { get; }

        // -(int32_t)write:(NSData *)data;
        [Export("write:")]
        int Write(NSData data);

        // -(NSData *)read:(NSUInteger)bytesToRead;
        [Export("read:")]
        NSData Read(nuint bytesToRead);

        // -(BOOL)getParsedStatus:(StarPrinterStatus_2 *)starPrinterStatus level:(u_int32_t)level;
        [Export("getParsedStatus:level:")]
        unsafe bool GetParsedStatus(StarPrinterStatus_2 starPrinterStatus, uint level);

        // -(BOOL)getParsedStatus:(StarPrinterStatus_2 *)starPrinterStatus level:(u_int32_t)level timeoutSec:(__darwin_time_t)timeoutSec;
        [Export("getParsedStatus:level:timeoutSec:")]
        unsafe bool GetParsedStatus(StarPrinterStatus_2 starPrinterStatus, uint level, nint timeoutSec);

        // -(BOOL)retrieveFirmwareInformation;
        [Export("retrieveFirmwareInformation")]
        bool RetrieveFirmwareInformation { get; }

        // -(BOOL)retrieveDipSwitchInformation;
        [Export("retrieveDipSwitchInformation")]
        bool RetrieveDipSwitchInformation { get; }

        // -(BOOL)getOnlineStatus:(BOOL *)onlineStatus;
        [Export("getOnlineStatus:")]
        unsafe bool GetOnlineStatus(bool onlineStatus);

        // -(BOOL)beginCheckedBlock;
        [Export("beginCheckedBlock")]
        bool BeginCheckedBlock { get; }

        // -(BOOL)endCheckedBlock;
        [Export("endCheckedBlock")]
        bool EndCheckedBlock { get; }

        // -(void)close;
        [Export("close")]
        void Close();
    }

    // @interface ExternalAccessoryPort : NSObject <NSStreamDelegate>
    [BaseType(typeof(NSObject))]
    interface ExternalAccessoryPort : INSStreamDelegate
    {
        // @property (readonly, getter = isConnected) BOOL connected;
        [Export("connected")]
        bool Connected { [Bind("isConnected")] get; }

        // @property (readwrite) u_int32_t endCheckedBlockTimeoutMillis;
        [Export("endCheckedBlockTimeoutMillis")]
        uint EndCheckedBlockTimeoutMillis { get; set; }

        // @property (assign, nonatomic) NSInteger dataTimeoutSeconds;
        [Export("dataTimeoutSeconds")]
        nint DataTimeoutSeconds { get; set; }

        // @property (readonly, retain) NSString * firmwareInformation;
        [Export("firmwareInformation", ArgumentSemantic.Retain)]
        string FirmwareInformation { get; }

        // @property (readonly, retain) NSMutableArray * dipSwitchInformation;
        [Export("dipSwitchInformation", ArgumentSemantic.Retain)]
        NSMutableArray DipSwitchInformation { get; }

        // @property (readonly, assign) BOOL isDKAirCash;
        [Export("isDKAirCash")]
        bool IsDKAirCash { get; }

        // @property (retain) NSThread * thread71;
        [Export("thread71", ArgumentSemantic.Retain)]
        NSThread Thread71 { get; set; }

        // @property (retain) NSNumber * result71;
        [Export("result71", ArgumentSemantic.Retain)]
        NSNumber Result71 { get; set; }

        // @property (readonly, assign) float secShortInterval;
        [Export("secShortInterval")]
        float SecShortInterval { get; }

        // @property (readonly, assign) float secInterval;
        [Export("secInterval")]
        float SecInterval { get; }

        // -(id)initWithPortName:(NSString *)portName portSettings:(NSString *)portSettings timeout:(u_int32_t)timeout emulation:(SMEmulation)emulation;
        [Export("initWithPortName:portSettings:timeout:emulation:")]
        IntPtr Constructor(string portName, string portSettings, uint timeout, SMEmulation emulation);

        // -(BOOL)open;
        [Export("open")]
        bool Open { get; }

        // -(int)write:(NSData *)data;
        [Export("write:")]
        int Write(NSData data);

        // -(NSData *)read:(NSUInteger)bytesToRead;
        [Export("read:")]
        NSData Read(nuint bytesToRead);

        // -(BOOL)getParsedStatus:(StarPrinterStatus_2 *)starPrinterStatus level:(u_int32_t)level;
        [Export("getParsedStatus:level:")]
        unsafe bool GetParsedStatus(StarPrinterStatus_2 starPrinterStatus, uint level);

        // -(BOOL)getOnlineStatus:(BOOL *)onlineStatus;
        [Export("getOnlineStatus:")]
        unsafe bool GetOnlineStatus(bool onlineStatus);

        // -(BOOL)retrieveFirmwareInformation;
        [Export("retrieveFirmwareInformation")]
        bool RetrieveFirmwareInformation { get; }

        // -(BOOL)retrieveFirmwareInformation2;
        [Export("retrieveFirmwareInformation2")]
        bool RetrieveFirmwareInformation2 { get; }

        // -(BOOL)retrieveDipSwitchInformation;
        [Export("retrieveDipSwitchInformation")]
        bool RetrieveDipSwitchInformation { get; }

        // -(NSInteger)retrieveButtonSecurityTimeout;
        [Export("retrieveButtonSecurityTimeout")]
        nint RetrieveButtonSecurityTimeout { get; }

        // -(BOOL)beginCheckedBlock:(StarPrinterStatus_2 *)starPrinterStatus level:(u_int32_t)level;
        [Export("beginCheckedBlock:level:")]
        unsafe bool BeginCheckedBlock(StarPrinterStatus_2 starPrinterStatus, uint level);

        // -(BOOL)endCheckedBlock:(StarPrinterStatus_2 *)starPrinterStatus level:(u_int32_t)level;
        [Export("endCheckedBlock:level:")]
        unsafe bool EndCheckedBlock(StarPrinterStatus_2 starPrinterStatus, uint level);

        // -(void)close;
        [Export("close")]
        void Close();

        // -(BOOL)disconnect;
        [Export("disconnect")]
        bool Disconnect { get; }

        // -(BOOL)startDataCancel;
        [Export("startDataCancel")]
        bool StartDataCancel { get; }
    }

    //// @interface Lock : NSObject
    //[BaseType(typeof(NSObject))]
    //interface Lock
    //{

    //}

    // @interface PortException : NSException
    [BaseType(typeof(NSException))]
    interface PortException
    {
    }

    // @interface PortInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface PortInfo
    {
        // -(id)initWithPortName:(NSString *)portName_ macAddress:(NSString *)macAddress_ modelName:(NSString *)modelName_;
        [Export("initWithPortName:macAddress:modelName:")]
        IntPtr Constructor(string portName_, string macAddress_, string modelName_);

        // @property (readonly, retain) NSString * portName;
        [Export("portName", ArgumentSemantic.Retain)]
        string PortName { get; }

        // @property (readonly, retain) NSString * macAddress;
        [Export("macAddress", ArgumentSemantic.Retain)]
        string MacAddress { get; }

        // @property (readonly, retain) NSString * modelName;
        [Export("modelName", ArgumentSemantic.Retain)]
        string ModelName { get; }
    }

    // @interface SMPort : NSObject
    [BaseType(typeof(NSObject))]
    interface SMPort
    {
        //// +(Lock *)sharedManager;
        //[Static]
        //[Export("sharedManager")]
        //Lock SharedManager { get; }

        //// -(void)lock;
        //[Export("lock")]
        //void Lock();

        //// -(void)unlock;
        //[Export("unlock")]
        //void Unlock();
        // @property (assign, readwrite, nonatomic) u_int32_t endCheckedBlockTimeoutMillis;
        [Export("endCheckedBlockTimeoutMillis")]
        uint EndCheckedBlockTimeoutMillis { get; set; }

        // +(NSString *)StarIOVersion;
        [Static]
        [Export("StarIOVersion")]
        string StarIOVersion { get; }

        // -(id)init:(NSString *)portName :(NSString *)portSettings :(u_int32_t)ioTimeoutMillis;
        [Export("init:::")]
        IntPtr Constructor(string portName, string portSettings, uint ioTimeoutMillis);

        // +(NSArray *)searchPrinter;
        [Static]
        [Export("searchPrinter")]
        PortInfo[] SearchPrinter();

        // +(NSArray *)searchPrinter:(NSString *)target;
        [Static]
        [Export("searchPrinter:")]
        PortInfo[] SearchPrinter(string target);

        // +(SMPort *)getPort:(NSString *)portName :(NSString *)portSettings :(u_int32_t)ioTimeoutMillis;
        [Static]
        [Export("getPort:::")]
        SMPort GetPort(string portName, string portSettings, uint ioTimeoutMillis);

        // +(void)releasePort:(SMPort *)port;
        [Static]
        [Export("releasePort:")]
        void ReleasePort(SMPort port);

        // -(u_int32_t)writePort:(const u_int8_t *)writeBuffer :(u_int32_t)offSet :(u_int32_t)size;
        [Export("writePort:::")]
        unsafe uint WritePort(byte writeBuffer, uint offSet, uint size);

        // -(u_int32_t)readPort:(u_int8_t *)readBuffer :(u_int32_t)offSet :(u_int32_t)size;
        [Export("readPort:::")]
        unsafe uint ReadPort(byte readBuffer, uint offSet, uint size);

        // -(void)getParsedStatus:(void *)starPrinterStatus :(u_int32_t)level;
        [Export("getParsedStatus::")]
        unsafe void GetParsedStatus(ref StarPrinterStatus_2 starPrinterStatus, uint level);

        // -(NSDictionary *)getFirmwareInformation;
        [Export("getFirmwareInformation")]
        NSDictionary FirmwareInformation { get; }

        // -(NSDictionary *)getDipSwitchInformation;
        [Export("getDipSwitchInformation")]
        NSDictionary DipSwitchInformation { get; }

        // -(_Bool)getOnlineStatus;
        [Export("getOnlineStatus")]
        bool OnlineStatus();

        // -(void)beginCheckedBlock:(void *)starPrinterStatus :(u_int32_t)level;
        [Export("beginCheckedBlock::")]
        unsafe void BeginCheckedBlock(ref StarPrinterStatus_2 starPrinterStatus, uint level);

        // -(void)endCheckedBlock:(void *)starPrinterStatus :(u_int32_t)level;
        [Export("endCheckedBlock::")]
        unsafe void EndCheckedBlock(ref StarPrinterStatus_2 starPrinterStatus, uint level);

        // -(BOOL)disconnect;
        [Export("disconnect")]
        bool Disconnect { get; }

        // +(NSMutableData *)compressRasterData:(int32_t)width :(int32_t)height :(u_int8_t *)imageData :(NSString *)portSettings;
        [Static]
        [Export("compressRasterData::::")]
        unsafe NSMutableData CompressRasterData(int width, int height, byte imageData, string portSettings);

        // +(NSMutableData *)generateBitImageCommand:(int32_t)width :(int32_t)height :(u_int8_t *)imageData :(NSString *)portSettings __attribute__((deprecated("")));
        [Static]
        [Export("generateBitImageCommand::::")]
        unsafe NSMutableData GenerateBitImageCommand(int width, int height, byte imageData, string portSettings);

        // -(NSString *)portName;
        [Export("portName")]
        string PortName { get; }

        // -(NSString *)portSettings;
        [Export("portSettings")]
        string PortSettings { get; }

        // -(u_int32_t)timeoutMillis;
        [Export("timeoutMillis")]
        uint TimeoutMillis { get; }

        // -(BOOL)connected;
        [Export("connected")]
        bool Connected { get; }

        // +(void)setMACAddressSourceBlock:(NSString *(^)(EAAccessory *))macAddressSourceBlock;
        [Static]
        [Export("setMACAddressSourceBlock:")]
        void SetMACAddressSourceBlock(Func<EAAccessory, NSString> macAddressSourceBlock);

        // -(u_int32_t)writePort:(const u_int8_t *)writeBuffer :(u_int32_t)offSet :(u_int32_t)size :(NSError **)error;
        [Export("writePort::::")]
        unsafe uint WritePort(byte writeBuffer, uint offSet, uint size, out NSError error);

        // -(u_int32_t)readPort:(u_int8_t *)readBuffer :(u_int32_t)offSet :(u_int32_t)size :(NSError **)error;
        [Export("readPort::::")]
        unsafe uint ReadPort(byte readBuffer, uint offSet, uint size, out NSError error);

        // -(id)getParsedStatus:(void *)starPrinterStatus :(u_int32_t)level :(NSError **)error;
        [Export("getParsedStatus:::")]
        unsafe NSObject GetParsedStatus(ref StarPrinterStatus_2 starPrinterStatus, uint level, out NSError error);

        // -(NSDictionary *)getFirmwareInformation:(NSError **)error;
        [Export("getFirmwareInformation:")]
        NSDictionary GetFirmwareInformation(out NSError error);

        // -(NSDictionary *)getDipSwitchInformation:(NSError **)error;
        [Export("getDipSwitchInformation:")]
        NSDictionary GetDipSwitchInformation(out NSError error);

        // -(id)getOnlineStatusWithError:(NSError **)error;
        [Export("getOnlineStatusWithError:")]
        NSObject GetOnlineStatusWithError(out NSError error);

        // -(id)beginCheckedBlock:(void *)starPrinterStatus :(u_int32_t)level :(NSError **)error;
        [Export("beginCheckedBlock:::")]
        unsafe NSObject BeginCheckedBlock(ref StarPrinterStatus_2 starPrinterStatus, uint level, out NSError error);

        // -(id)endCheckedBlock:(void *)starPrinterStatus :(u_int32_t)level :(NSError **)error;
        [Export("endCheckedBlock:::")]
        unsafe NSObject EndCheckedBlock(ref StarPrinterStatus_2 starPrinterStatus, uint level, out NSError error);
    }

}