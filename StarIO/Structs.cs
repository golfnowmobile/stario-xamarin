﻿using System.Runtime.InteropServices;
namespace StarIO{
    
[StructLayout (LayoutKind.Sequential)]
public struct StarPrinterStatus_0
{
    public int coverOpen;

    public int offline;

    public int compulsionSwitch;

    public int overTemp;

    public int unrecoverableError;

    public int cutterError;

    public int mechError;

    public int pageModeCmdError;

    public int paperSizeError;

    public int presenterPaperJamError;

    public int headUpError;

    public int blackMarkDetectStatus;

    public int paperEmpty;

    public int paperNearEmptyInner;

    public int paperNearEmptyOuter;

    public int stackerFull;

    public int etbAvailable;

    public int etbCounter;

    public int presenterState;

    public int rawLength;

    public int[] raw;
}

[StructLayout (LayoutKind.Sequential)]
public struct StarPrinterStatus_1
{
    public int coverOpen;

    public int offline;

    public int compulsionSwitch;

    public int overTemp;

    public int unrecoverableError;

    public int cutterError;

    public int mechError;

    public int receiveBufferOverflow;

    public int pageModeCmdError;

    public int blackMarkError;

    public int presenterPaperJamError;

    public int headUpError;

    public int receiptBlackMarkDetection;

    public int receiptPaperEmpty;

    public int receiptPaperNearEmptyInner;

    public int receiptPaperNearEmptyOuter;

    public int presenterPaperPresent;

    public int peelerPaperPresent;

    public int stackerFull;

    public int slipTOF;

    public int slipCOF;

    public int slipBOF;

    public int validationPaperPresent;

    public int slipPaperPresent;

    public int etbAvailable;

    public int etbCounter;

    public int presenterState;

    public int rawLength;

    public int[] raw;
}

[StructLayout (LayoutKind.Sequential)]
public struct StarPrinterStatus_2
{
    public int coverOpen;

    public int offline;

    public int compulsionSwitch;

    public int overTemp;

    public int unrecoverableError;

    public int cutterError;

    public int mechError;

    public int headThermistorError;

    public int receiveBufferOverflow;

    public int pageModeCmdError;

    public int blackMarkError;

    public int presenterPaperJamError;

    public int headUpError;

    public int voltageError;

    public int receiptBlackMarkDetection;

    public int receiptPaperEmpty;

    public int receiptPaperNearEmptyInner;

    public int receiptPaperNearEmptyOuter;

    public int presenterPaperPresent;

    public int peelerPaperPresent;

    public int stackerFull;

    public int slipTOF;

    public int slipCOF;

    public int slipBOF;

    public int validationPaperPresent;

    public int slipPaperPresent;

    public int etbAvailable;

    public int etbCounter;

    public int presenterState;

    public int rawLength;

    public int[] raw;
}

static class CFunctions
{
    // extern int GetEndCheckedBlockTimeoutMillis (void *port);
    [DllImport ("__Internal")]
    static extern unsafe int GetEndCheckedBlockTimeoutMillis (void* port);

    // extern void SetEndCheckedBlockTimeoutMillis (void *port, int timeoutMillis);
    [DllImport ("__Internal")]
    static extern unsafe void SetEndCheckedBlockTimeoutMillis (void* port, int timeoutMillis);
}

public enum SMEmulation : uint
{
    Unknown = 0,
    StarLine,
    Escpos
}

}